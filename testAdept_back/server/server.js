import exp from 'express';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let app = exp();

app.use(exp.json());

app.use((req, res, next) => {
    req.srcFolder = '/ds/';
    req.src = 'rows.json';
    req.filePath = path.join(__dirname);
    next();
});

app.get('/rows/:from/:to', checkPathExist, (req, res, next) => {
    req.body.from = +req.params.from;
    req.body.to = +req.params.to;
    next();
}, getFileR);

app.get('/rows/:from', checkPathExist, (req, res, next) => {
    req.body.from = +req.params.from;
    next();
}, checkLength);

app.get('/rows', checkPathExist, (req, res, next) => {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        const dat = JSON.parse(data);
        res.send(`${dat.length}`);
    });
});

app.post('/rows', mkDirectory, checkFileExist, fileRW);

app.put('/rows/:lastIndex', (req, res, next) => {
    req.body.lastIndex = +req.params.lastIndex;
    next();
}, parseArray, deleteFileRW);

app.patch('/rows/:id', (req, res, next) => {
    req.body.id = +req.params.id;
    next();
}, editFileRW);

app.get('/emprows/:company/:from', (req, res, next) => {
    req.body.company = +req.params.company;
    req.body.from = +req.params.from;
    next();
}, checkLength);

app.get('/emprows/:company/:from/:to', (req, res, next) => {
    req.body.company = +req.params.company;
    req.body.from = +req.params.from;
    req.body.to = +req.params.to;
    next();
}, obtainEmps, getFileR);

app.get('/emprows/count', (req, res, next) => {
    req.body.company = +req.query.company;
    next();
}, obtainEmps, (req, res) => {
    res.send(JSON.stringify(req.body.emps));
});

app.get('/emprows/:company', checkPathExist, (req, res, next) => {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        const dat = JSON.parse(data);
        for (let item of dat) {
            if (item.id === +req.params.company) {
                res.send(`${item.employees.length}`);
                break;
            }
        }
    });
});

app.post('/emprows', obtainEmps, defineId, fileRW);

app.put('/emprows/:lastIndex', (req, res, next) => {
    req.body.lastIndex = +req.params.lastIndex;
    next();
}, obtainEmps, parseArray, deleteFileRW);

app.listen(3001, (err) => {
    err ? console.log('Error:' + err) : console.log(`It' fine`);
});

app.use((req, res) => {
    if (req.method === 'GET') {
        if (req.url === '/')
            loadPage(res, 'index.html', 'text/html');
        else if (!req.url.match('rows')) {
            loadPage(res, req.url, getContentType(req.url));
        }
    }
});

function checkPathExist(req, res, next) {
    fs.readdir(req.filePath, (err, files) => {
        err && next(err);
        if (files.includes('ds')) {
            fs.readdir(path.join(req.filePath + req.srcFolder), (err, files) => {
                err && next(err);
                !files.includes(req.src) && next('No such file, create a row to start working');
                next();
            });
        }
        else
            next('No such directory, create a row to start working');
    });
}

function getFileR(req, res) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        if (err) {
            res.writeHead(404);
            if (req.url.match('rows')) {
                res.write('No rows yet');
            }
        }
        else {
            const dat = ('emps' in req.body ? req.body.emps : JSON.parse(data)).filter((_, index) => (index >= req.body.from && index < req.body.to));
            res.write(JSON.stringify(dat));
        }
        res.end();
    });
}

function mkDirectory(req, res, next) {
    fs.mkdir(path.join(req.filePath + req.srcFolder), (err) => {
        if (err) {
            err.code === 'EEXIST' ? console.log('Folder already exists') : next(err);
        }
    });
    next();
}

function checkFileExist(req, res, next) {
    fs.readdir(req.filePath + req.srcFolder, (err, files) => {
        err && next(err);
        if (files.includes(req.src)) {
            defineId(req, res, next);
        }
        else {
            createFileW(req, res, next);
        }
    });
}

function obtainEmps(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        for (let item of JSON.parse(data)) {
            if (item.id === req.body.company) {
                req.body.emps = [...item.employees];
            }
        }
        next();
    });
}

function defineId(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        const dat = req.url.match(/emp/) ? req.body.emps : JSON.parse(data);
        let maxId = 0;
        if (dat.length === 0)
            req.body.id = 0;
        else {
            for (let item of dat) {
                item.id > maxId && (maxId = item.id);
            }
            for (let i = 0; i < maxId; i++) {
                if (!(dat.filter(item => (item.id === i)).length)) {
                    req.body.id = i;
                    break;
                }
            }
            !('id' in req.body) && (req.body.id = maxId + 1);
        }
        next();
    });
}

function parseArray(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        const dat = req.url.match(/emp/) ? req.body.emps : JSON.parse(data);
        req.body.parsedArr = [];
        if (dat.length > 1) {
            for (let item of dat) {
                let fl = 1;
                for (let im of req.body.highlighted) {
                    if (item.id === im) {
                        fl = 0;
                        break;
                    }
                }
                if (fl) {
                    req.body.parsedArr.push(item);
                }
            }
        }
        next()
    });
}

function createFileW(req, res, next) {
    req.body.id = 0;
    fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify([req.body]), (err) => {
        err && next(err);
        res.send(JSON.stringify({ idx: 0, elem: req.body }))
    });
}

function fileRW(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        const dat = JSON.parse(data);
        if ('emps' in req.body) {
            const objBuf = {};
            for (let key in req.body) {
                if (key !== 'emps')
                    objBuf[key] = req.body[key];
            }
            for (let item of dat) {
                if (item.id === req.body.company) {
                    item.employees.push(objBuf);
                    item.quant = item.employees.length;
                }
            }
        }
        else {
            dat.push(req.body);
        }
        fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify(dat), (err) => {
            if (err) next(err);
        });
        if ('emps' in req.body) {
            for (let item of dat)
                item.id === req.body.company && res.send(JSON.stringify({ idx: item.employees.length - 1, elem: req.body }));
        }
        else
            res.send(JSON.stringify({ idx: dat.length - 1, elem: req.body }));
    });
}

function deleteFileRW(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        let dat = JSON.parse(data);
        let objLink;
        if ('emps' in req.body) {
            for (let item of dat) {
                if (item.id === req.body.company) {
                    objLink = item;
                    item.employees = [...req.body.parsedArr];
                    item.quant = item.employees.length;
                    break;
                }
            }
        }
        else
            dat = [...req.body.parsedArr];
        fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify(dat), (err) => {
            if (err) next(err);
        });
        const arrBuf = [];
        if ('emps' in req.body) {
            if (objLink.employees.length >= req.body.lastIndex) {
                for (let i = 0; i < req.body.lastIndex; i++)
                    arrBuf.push(objLink.employees[i]);
                res.send(JSON.stringify(arrBuf));
            }
            else
                res.send(JSON.stringify(objLink.employees));
        }
        else {
            if (dat.length >= req.body.lastIndex) {
                for (let i = 0; i < req.body.lastIndex; i++)
                    arrBuf.push(dat[i]);
                res.send(JSON.stringify(arrBuf));
            }
            else
                res.send(JSON.stringify(dat));
        }
    });
}

function editFileRW(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        const dat = JSON.parse(data);
        if ('surname' in req.body) {
            for (let item of dat) {
                if (item.id === req.body.company) {
                    for (let im of item.employees) {
                        if (im.id === +req.body.id) {
                            for (let key in req.body) {
                                im[key] = req.body[key];
                            }
                            break;
                        }
                    }
                }
            }
        }
        else {
            for (let item of dat) {
                if (item.id === +req.body.id) {
                    for (let key in req.body) {
                        if (key !== 'employees' && key !== 'quant')
                            item[key] = req.body[key];
                    }
                    break;
                }
            }
        }
        fs.writeFile(path.join(req.filePath + req.srcFolder + req.src), JSON.stringify(dat), (err) => {
            if (err) next(err);
        });
        if ('surname' in req.body) {
            for (let item of dat) {
                if (item.id === req.body.company)
                    res.send(JSON.stringify(item.employees));
            }
        }
        else
            res.send(JSON.stringify(dat));
    });
}

function checkLength(req, res, next) {
    fs.readFile(path.join(req.filePath + req.srcFolder + req.src), { encoding: 'utf-8' }, (err, data) => {
        err && next(err);
        let fl = 0;
        const dat = JSON.parse(data);
        if (req.url.match(/emp/)) {
            for (let item of dat) {
                if (item.id === req.body.company) {
                    if (item.employees.length >= req.body.from + 1) {
                        fl = 1;
                        break;
                    }
                }
            }
        }
        else {
            if (dat.length >= req.body.from + 1)
                fl = 1;
        }
        res.send(`${fl}`);
    });
}

app.use(errorHandler);

function errorHandler(err, req, res, next) {
    res.writeHead(500);
    res.write(err);
    res.end();
}

function loadPage(res, name, conType) {
    const file = path.join(__dirname + '/../' + 'build/' + name);
    fs.readFile(file, (err, data) => {
        if (err)
            res.writeHead(404);
        else {
            res.writeHead(200, { 'Content-Type': conType });
            res.write(data);
        }
        res.end();
    })
}

function getContentType(url) {
    let ext = path.extname(url);
    switch (ext) {
        case '.html':
            return 'text/html';
        case '.css':
            return 'text/css';
        case '.js':
            return 'text/javascript';
        case '.json':
            return 'application/json';
        default:
            return 'multipart/form-data';
    }
}