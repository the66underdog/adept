import React, { useState } from 'react';
import './App.css';
import EmpTable from './Components/EmpTable';
import Table from './Components/Table';

function App() {

  const [empRows, setEmpRows] = useState({ company: [], emps: null });

  return (
    <main className='main'>
      <Table empRows={empRows} setEmpRows={setEmpRows} />
      {empRows.emps !== null && <EmpTable emp={true} rows={empRows} setRows={setEmpRows} />}
    </main>
  );
}

export default App;
