import './style.css';

function AddForm({ emp, lastRowIndex, setRows, setAll, checkInputs }) {

    function postRow(e) {
        const obj = checkInputs(e.currentTarget);
        const req = new XMLHttpRequest();
        req.open('POST', emp ? '/emprows' : '/rows', true);
        req.onload = () => {
            if (req.status === 200) {
                if (lastRowIndex > JSON.parse(req.response).idx) {
                    emp ? setRows(prev => ({ ...prev, emps: [...prev.emps, JSON.parse(req.response).elem], all: prev.all + 1 })) :
                        setRows(prev => [...prev, JSON.parse(req.response).elem]);
                }
                !emp && setAll(prev => prev + 1);
            }
        }
        req.onerror = err => { throw err = new Error() };
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify(obj));
    }

    return (
        <tr className='tr-add'>
            <td>
                <form id={emp ? 'form-add-emps' : 'form-add'} onSubmit={e => { e.preventDefault(); postRow(e); e.currentTarget.reset(); e.currentTarget.elements[1].focus() }}
                    autoComplete='off' />
                <button className='btn-submit' type='submit' form={emp ? 'form-add-emps' : 'form-add'} />
            </td>
            <td><input type='text' form={emp ? 'form-add-emps' : 'form-add'} name={emp ? 'surname' : 'company'} placeholder={emp ? 'Surname...' : 'Company...'} autoFocus autoComplete='off' /></td>
            <td><input type='text' form={emp ? 'form-add-emps' : 'form-add'} name={emp ? 'name' : null} placeholder={emp ? 'Name...' : null} disabled={emp ? false : true} /></td>
            <td><input type='text' form={emp ? 'form-add-emps' : 'form-add'} name={emp ? 'pos' : 'adress'} placeholder={emp ? 'Position...' : 'Adress...'} autoComplete='off' /></td>
        </tr>
    );
}

export default AddForm;