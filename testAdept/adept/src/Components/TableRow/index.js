import React, { useEffect, useState } from 'react';
import EditRow from '../EditRow';
// import './style.css';

function TableRow({ emp, row, quant, setRows, trStyle, setHighlighted, checkInputs, idx }) {

    const [editRow, setEditRow] = useState(false);

    function postEditRow(e) {
        const obj = checkInputs(e.currentTarget);
        const req = new XMLHttpRequest();
        req.open('PATCH', `/rows/${row.id}`, true);
        req.onload = () => {
            if (req.status === 200) {
                emp ? setRows(prev => ({ ...prev, emps: JSON.parse(req.response).filter((_, i) => (i < prev.emps.length)) })) :
                    setRows(prev => (JSON.parse(req.response).filter((_, i) => (i < prev.length))));
                setEditRow(false);
            }
        }
        req.onerror = err => { throw err = new Error() };
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify(obj));
    }

    function handleTrClick(e) {
        if (!e.target.closest('input[type=checkbox'))
            setEditRow(<EditRow key={row.id} emp={emp} row={row} h={e.currentTarget.offsetHeight - 2} postEditRow={postEditRow} mult={idx} />);
    }

    function hideEditRow(e) {
        if (!e.target.closest('.tr-edit'))
            setEditRow(null);
    }

    function toggleRow(e) {
        if (emp)
            e.target.checked ? setHighlighted(prev => [...prev, row.id]) : setHighlighted(prev => prev.filter(item => (item !== row.id)));
        else {
            e.target.checked ? setHighlighted(prev => ({ ...prev, company: [...prev.company, row.id] })) :
                setHighlighted(prev => ({ ...prev, company: prev.company.filter(item => (item !== row.id)) }));
        }
    }

    useEffect(() => {
        editRow ? document.addEventListener('mousedown', hideEditRow) :
            document.removeEventListener('mousedown', hideEditRow)
    }, [editRow]);

    return (
        <>
            <tr style={trStyle} onClick={(e) => { handleTrClick(e) }} >
                <td><input type='checkbox' onChange={e => toggleRow(e)} checked={trStyle ? true : false} /></td>
                <td>{emp ? row.surname : row.company}</td>
                <td>{emp ? row.name : quant}</td>
                <td>{emp ? row.pos : row.adress}</td>
            </tr>
            {editRow}
        </>
    );
}

export default TableRow;