import React, { useCallback, useEffect, useMemo, useState } from 'react';
import AddForm from '../AddForm';
import TableHead from '../TableHead';
import TablePanel from '../TablePanel';
import TableRow from '../TableRow';
import './style.css';

const MemoTableRow = React.memo(TableRow);

function Table({ emp = false, empRows, setEmpRows }) {

    const [rows, setRows] = useState([]);
    const [addForm, setAddForm] = useState(false);
    const [all, setAll] = useState(0);

    const [currentPage, setCurrentPage] = useState(1);
    const rowAmountPerPage = 20;
    const lastRowIndex = currentPage * rowAmountPerPage;
    const firstRowIndex = lastRowIndex - rowAmountPerPage;

    function hideAddForm(e) {
        if (e.target.closest('.tr-add'))
            return;
        if (!e.target.closest('.btn-add'))
            setAddForm(false);
    }

    const memoCheckInputs = useCallback((form) => {
        const formData = new FormData(form);
        return {
            company: formData.get('company').trim(),
            adress: formData.get('adress').trim(),
            quant: 0,
            employees: [],
        };
    }, []);

    useEffect(() => {
        const req = new XMLHttpRequest();
        req.open('GET', `/rows/${firstRowIndex}/${lastRowIndex}`, true);
        req.onload = () => {
            if (req.status === 200) {
                setRows(prev => [...prev, ...JSON.parse(req.response)]);
            }
            else {
                console.log(req.response);
            }
        }
        req.onerror = err => { throw err = new Error() };
        req.setRequestHeader('Content-Type', 'application/json');
        req.send();
        return () => (req.abort());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentPage]);

    useEffect(() => {
        const controller = new AbortController();
        async function getAmount() {
            try {
                const res = await fetch('/rows', {
                    signal: controller.signal
                });
                const result = await res.text();
                if (res.ok) {
                    setAll(+result);
                }
                else {
                    console.log(result);
                }
            }
            catch (err) {
                if (err.name === 'AbortError')
                    console.log(err);
            }
        }
        getAmount();
        return () => { controller.abort() };
    }, []);

    useEffect(() => {
        addForm ? document.addEventListener('mousedown', hideAddForm) :
            document.removeEventListener('mousedown', hideAddForm);
    }, [addForm]);

    useEffect(() => {
        if (empRows.company.length === 1) {
            for (let item of rows) {
                if (item.id === empRows.company[0]) {
                    setEmpRows(prev => ({ ...prev, emps: [] }));
                    break;
                }
            }
        }
        return () => (setEmpRows(prev => ({ ...prev, emps: null })));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [empRows.company]);

    useEffect(() => {
        if ('all' in empRows) {
            const arrBuf = [...rows];
            for (let item of arrBuf) {
                if (item.id === empRows.company[0]) {
                    item.quant = empRows.all;
                    break;
                }
            }
            setRows([...arrBuf]);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [empRows]);

    const trStyle = useMemo(() => {
        return { backgroundColor: 'orange', color: 'white' };
    }, []);

    return (
        <div className='table-container'>
            <TablePanel setRows={setRows} setAddForm={setAddForm} lastRowIndex={lastRowIndex} setAll={setAll} highlighted={empRows.company} setHighlighted={setEmpRows}
                currentPage={currentPage} setCurrentPage={setCurrentPage} rowAmountPerPage={rowAmountPerPage} len={rows.length} all={all} />
            <table className='table-main'>
                <TableHead emp={emp} rows={rows} setHighlighted={setEmpRows} />
                <tbody>
                    {rows.map((item, index) => (<MemoTableRow key={item.id} emp={emp} row={item} quant={item.quant} setRows={setRows} trStyle={empRows.company.includes(item.id) ? trStyle : null}
                        setHighlighted={setEmpRows} checkInputs={memoCheckInputs} idx={index} />))}
                    {addForm && <AddForm emp={emp} lastRowIndex={lastRowIndex} setRows={setRows} setAll={setAll} checkInputs={memoCheckInputs} />}
                </tbody>
            </table>
        </div>
    );
}

export default Table;