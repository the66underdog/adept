import './style.css';

function EditRow({ emp, row, h, postEditRow, mult }) {

    function parseInputs(e) {
        for (let item of e.currentTarget.elements) {
            if (item.value.trim() === '') {
                item.value = item.placeholder;
            }
        }
    }

    return (
        <tr className='tr-edit' style={{ top: `${mult * h}px` }}>
            <td>
                <form id='form-edit' onSubmit={e => { e.preventDefault(); parseInputs(e); postEditRow(e) }} autoComplete='off' />
                <button className='btn-submit' type='submit' form='form-edit' />
            </td>
            <td><input type='text' form='form-edit' name={emp ? 'surname' : 'company'}
                placeholder={emp ? row.surname : row.company} autoFocus autoComplete='off' /></td>
            <td><input type='text' form='form-edit' name={emp ? 'name' : 'quant'} placeholder={emp ? row.name : 'Auto'}
                disabled={'name' in row ? false : true} autoComplete='off' /></td>
            <td><input type='text' form='form-edit' name={emp ? 'pos' : 'adress'} placeholder={emp ? row.pos : row.adress} autoComplete='off' /></td>
        </tr>
    );
}

export default EditRow;