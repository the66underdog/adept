import React from 'react';
import './style.css';

function TableHead({ emp, rows, setHighlighted }) {

    function toggleCheckbox(e) {
        if (emp) {
            e.currentTarget.checked ? setHighlighted(rows.map(item => item.id)) : setHighlighted([]);
        }
        else {
            e.currentTarget.checked ? setHighlighted(prev => ({ ...prev, company: rows.map(item => item.id) })) : setHighlighted(prev => ({ ...prev, company: [] }));
        }
    }

    return (
        <thead>
            <tr>
                <th><input type='checkbox' onChange={(e) => { toggleCheckbox(e) }} /></th>
                <th>{emp ? 'Фамилия' : 'Название компании'}</th>
                <th>{emp ? 'Имя' : 'Количество сотрудников'}</th>
                <th>{emp ? 'Должность' : 'Адрес'}</th>
            </tr>
        </thead>
    );
}

export default TableHead;