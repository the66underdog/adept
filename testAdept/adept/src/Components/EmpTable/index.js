import React, { useCallback, useEffect, useMemo, useState } from 'react';
import AddForm from '../AddForm';
import TableHead from '../TableHead';
import TablePanel from '../TablePanel';
import TableRow from '../TableRow';

const MemoTableRow = React.memo(TableRow);

function EmpTable({ emp, rows, setRows }) {

    const [addForm, setAddForm] = useState(false);
    const [highlighted, setHighlighted] = useState([]);

    const [currentPage, setCurrentPage] = useState(1);
    const rowAmountPerPage = 20;
    const lastRowIndex = currentPage * rowAmountPerPage;
    const firstRowIndex = lastRowIndex - rowAmountPerPage;

    const memoHighlighted = useMemo(() => (highlighted), [highlighted]);

    function hideAddForm(e) {
        if (e.target.closest('.tr-add'))
            return;
        if (!e.target.closest('.btn-add'))
            setAddForm(false);
    }

    const memoCheckInputs = useCallback((form) => {
        const formData = new FormData(form);
        return {
            surname: formData.get('surname').match(/\d/) ? null : formData.get('surname').trim(),
            name: formData.get('name').match(/\d/) ? null : formData.get('name').trim(),
            pos: formData.get('pos').trim(),
            company: rows.company[0],
        };
    }, [rows.company]);

    useEffect(() => {
        const controller = new AbortController();
        async function getAmount() {
            try {
                const res = await fetch(`/emprows/${rows.company}`, {
                    signal: controller.signal
                });
                const result = await res.text();
                if (res.ok) {
                    setRows(prev => ({ ...prev, all: +result }));
                }
                else {
                    console.log(result);
                }
            }
            catch (err) {
                if (err.name === 'AbortError')
                    console.log(err);
            }
        }
        getAmount();
        return () => { controller.abort() };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        const req = new XMLHttpRequest();
        req.open('GET', `emprows/${rows.company}/${firstRowIndex}/${lastRowIndex}`, true);
        req.onload = () => {
            if (req.status === 200) {
                JSON.parse(req.response).length && setRows(prev => ({ ...prev, emps: [...prev.emps, ...JSON.parse(req.response)] }));
            }
        }
        req.onerror = err => { throw err = new Error() };
        req.setRequestHeader('Content-Type', 'application/json');
        req.send();
        return () => (req.abort());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentPage]);

    useEffect(() => {
        addForm ? document.addEventListener('mousedown', hideAddForm) :
            document.removeEventListener('mousedown', hideAddForm);
    }, [addForm]);

    const trStyle = useMemo(() => {
        return { backgroundColor: 'orange', color: 'white' };
    }, []);

    return (
        <div className='table-container'>
            <TablePanel rows={rows} setRows={setRows} lastRowIndex={lastRowIndex} setAddForm={setAddForm} highlighted={memoHighlighted} setHighlighted={setHighlighted}
                currentPage={currentPage} setCurrentPage={setCurrentPage} rowAmountPerPage={rowAmountPerPage} len={rows.emps.length} all={rows.all} />
            <table className='table-main mirrored'>
                <TableHead emp={emp} rows={rows.emps} setHighlighted={setHighlighted} />
                <tbody>
                    {rows.emps.map((item, index) => (<MemoTableRow key={item.id} emp={emp} row={item} setRows={setRows} trStyle={highlighted.includes(item.id) ? trStyle : null}
                        setHighlighted={setHighlighted} checkInputs={memoCheckInputs} idx={index} />))}
                    {addForm && <AddForm emp={emp} lastRowIndex={lastRowIndex} setRows={setRows} checkInputs={memoCheckInputs} />}
                </tbody>
            </table>
        </div>
    );
}

export default EmpTable;