import React from 'react';
import './style.css';

function TablePanel({ rows = null, setRows, lastRowIndex, setAddForm, setAll = null, highlighted, setHighlighted,
    currentPage, setCurrentPage, rowAmountPerPage, len, all }) {

    function postDeleteRows() {
        const req = new XMLHttpRequest();
        req.open('PUT', rows ? `/emprows/${lastRowIndex}` : `/rows/${lastRowIndex}`, true);
        req.onload = () => {
            rows ? setRows(prev => ({ ...prev, emps: JSON.parse(req.response), all: prev.all - highlighted.length })) : setRows(JSON.parse(req.response));
            setAll && setAll(prev => prev - highlighted.length);
            rows ? setHighlighted([]) : setHighlighted({ company: [], emps: null });
        }
        req.onerror = err => { throw err = new Error() };
        req.setRequestHeader('Content-Type', 'application/json');
        const obj = rows ? { company: rows.company[0], highlighted: highlighted } : { highlighted: highlighted };
        req.send(JSON.stringify(obj));
    }

    async function testLoad() {
        const res = await fetch(`/rows/${(currentPage + 1) * rowAmountPerPage - rowAmountPerPage}`);
        const result = await res.text();
        if (res.ok) {
            +result && setCurrentPage(prev => prev + 1);
        }
        else {
            console.log(result);
        }
    }

    return (
        <div className='table-panel'>
            <div className='table-panel__stats'>
                <button className='btn-load' onClick={() => { testLoad() }}>Next</button>
                <span className='table-panel__amount'>Rows : {len} of {all}</span>
            </div>
            <div className='table-panel__options'>
                <button className='btn-add' onClick={() => setAddForm(prev => !prev)}><span className='text-sign'>+</span> Add</button>
                <button className='btn-del' onClick={() => postDeleteRows()}
                    disabled={highlighted.length ? false : true}><span className='text-sign'>x</span> Delete</button>
            </div>
        </div>
    );
}

export default TablePanel;